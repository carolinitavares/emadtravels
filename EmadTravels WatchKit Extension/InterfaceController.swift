//
//  InterfaceController.swift
//  EmadTravels WatchKit Extension
//
//  Created by Carolini Freire Ardito Tavares on 2019-03-17.
//  Copyright © 2019 Carolini Freire Ardito Tavares. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
    @IBOutlet weak var allFlightsTable: WKInterfaceTable!
    var fromChoice = ""
    var toChoice = ""
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        // Play a "click" sound when you get the message
        WKInterfaceDevice().play(.click)
        
        // output a debug message to the terminal
        print("Got a message!")
        print("count = \(allFlights!.count)")
        
        self.fromChoice = message["from"] as! String
        self.toChoice = message["to"] as! String
        print(fromChoice)
        print(toChoice)
        if fromChoice != "" {
            self.popularArray()
            //self.popularTela()
        }
    }

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        
        self.popularTela()
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    func popularArray(){
        var forecastSpecificCall = "flights"
        let forecastService = ConnectAPI(specificCall: forecastSpecificCall)
        forecastService.getCurrentFlight(flyFrom: fromChoice, flyTo: toChoice, dateFrom: "18/03/2019", dateTo: "19/03/2019", currency: "CAD") { (currentFlight) in
            data = currentFlight as! [Flight]
            for (key, value) in data.enumerated() {
                allFlights?.append(value)
            }
            self.popularTela()
        }
    }
    
    func popularTela(){
        self.allFlightsTable.setNumberOfRows(allFlights!.count, withRowType:"myRow")
        
        
        print("number of rows: \(allFlights!.count)")
        // 1. loop through your array
        // 2. take each item in the array and put it in a table row
        for (i, flight) in allFlights!.enumerated() {
            
            let row = self.allFlightsTable.rowController(at: i) as! InterfaceRowController
            
            row.fromLabel.setText(flight.cityFrom!)
            row.toLabel.setText(flight.cityTo!)
            row.flightNboLabel.setText("Duration: \(flight.fly_duration as! String)")
            
        }
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        let flight = allFlights![rowIndex]
        let controllers = ["Flight", "CheckIn"]
        detailFlights = allFlights![rowIndex]
        presentController(withNames: controllers, contexts: [flight, flight])
    }
}
