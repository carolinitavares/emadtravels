//
//  InterfaceRowController.swift
//  EmadTravels WatchKit Extension
//
//  Created by Carolini Freire Ardito Tavares on 2019-03-18.
//  Copyright © 2019 Carolini Freire Ardito Tavares. All rights reserved.
//

import WatchKit

class InterfaceRowController: NSObject {
    
    @IBOutlet weak var fromLabel: WKInterfaceLabel!
    @IBOutlet weak var toLabel: WKInterfaceLabel!
    @IBOutlet weak var flightNboLabel: WKInterfaceLabel!    
    
}
