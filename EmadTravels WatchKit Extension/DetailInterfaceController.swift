//
//  DetailInterfaceController.swift
//  EmadTravels WatchKit Extension
//
//  Created by Carolini Freire Ardito Tavares on 2019-03-18.
//  Copyright © 2019 Carolini Freire Ardito Tavares. All rights reserved.
//

import WatchKit
import Foundation


class DetailInterfaceController: WKInterfaceController {

    
    @IBOutlet weak var fromToLabel: WKInterfaceLabel!
    @IBOutlet weak var flightNboLabel: WKInterfaceLabel!
    @IBOutlet weak var timaLabel: WKInterfaceLabel!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        /*if detailFlights = context as? Flight {
            detailFlights = Flight
        }*/
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        self.popularTela()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    @IBAction func checkInPressed() {
        
    }
    
    func popularTela(){
        
        print("number of rows: \(allFlights!.count)")
        print("number of rows2: \(detailFlights)")
        // 1. loop through your array
        // 2. take each item in the array and put it in a table row
        //fromToLabel.setText("\(detailFlights!.cityFrom) - \(detailFlights!.cityTo)")
    }
}
