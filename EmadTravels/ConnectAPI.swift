//
//  ConnectAPI.swift
//  EmadTravels
//
//  Created by Carolini Freire Ardito Tavares on 2019-03-18.
//  Copyright © 2019 Carolini Freire Ardito Tavares. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ConnectAPI {
    let forecastBaseURL: URL?
    let specificCall: String
    var jsonResponse = JSON()
    var flightsResponse = JSON()
    
    init(specificCall: String) {
        self.specificCall = specificCall
        self.forecastBaseURL = URL(string: "https://api.skypicker.com/\(specificCall)")
    }
    
    /*func getCurrentFlight(flyFrom: String, flyTo: String, dateFrom: String, dateTo: String, currency: String){
        var help = "\(forecastBaseURL)\(forecastBaseURL!)?flyFrom=\(flyFrom)&to=\(flyTo)&dateFrom=\(dateFrom)&dateTo=\(dateTo)&partner=picky&curr=\(currency)&max_stopovers=0"
        Alamofire.request(help).responseJSON {
            // 1. store the data from the internet in the
            // response variable
            response in
            
            // 2. get the data out of the variable
            guard let apiData = response.result.value
                else {
                    print("Error getting data from the URL")
                    return
            }
            
            // OUTPUT the json response to the terminal
            print("=================")
            // print(apiData)
            // GET something out of the JSON response
            self.jsonResponse = JSON(apiData)
            
            print(self.jsonResponse)
            self.flightsResponse = self.jsonResponse["items"]
            
            for (key, value1) in self.flightsResponse{
                
                // print(value1["depatureCity"])
                //self.toData.append(value1["depatureCity"].string!)
                //self.arrivalData.append(value1["arrivalCity"].string!)
                
            }
        }
    }*/
    
    
    func getCurrentFlight(flyFrom: String, flyTo: String, dateFrom: String, dateTo: String, currency: String, completion: @escaping ([Flight?]) -> Void ){
        if let forecastURL = URL(string: "\(forecastBaseURL!)?flyFrom=\(flyFrom)&to=\(flyTo)&dateFrom=\(dateFrom)&dateTo=\(dateTo)&partner=picky&curr=\(currency)&max_stopovers=0") {
            Alamofire.request(forecastURL).responseJSON{ (response) in
                if let jsonDictionary = response.result.value as? [String : Any] {
                    if let currentFlightDictionary = jsonDictionary["data"] as? [[String : Any]]  {
                        var data: [Flight]? = [] 
                        var index = 0
                        for _ in 1...currentFlightDictionary.count {
                            let currentFlight = Flight(JSON: currentFlightDictionary[index])
                            //print(currentFlight)
                            data?.append(currentFlight!)
                            index = index + 1
                        }
                        //print("dentro -> \(data!.count)")
                        //allFlights = data!
                        //print("all flights dentro -> \(allFlights)")
                        completion(data!)
                    } else {
                        completion([nil])
                    }
                }
            }
        }
        print("\(forecastBaseURL!)?flyFrom=\(flyFrom)&to=\(flyTo)&dateFrom=\(dateFrom)&dateTo=\(dateTo)&partner=picky&curr=\(currency)&max_stopovers=0" )
    }
}

