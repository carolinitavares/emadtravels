//
//  ViewController.swift
//  EmadTravels
//
//  Created by Carolini Freire Ardito Tavares on 2019-03-17.
//  Copyright © 2019 Carolini Freire Ardito Tavares. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    //var data: [Flight?] = []
    @IBOutlet weak var fromPicker: UIPickerView!
    @IBOutlet weak var toPicker: UIPickerView!
    
    let airportsPickerViewDataFrom = ["ATL", "PEK", "DXB", "LAX", "HND", "ORD", "LHR", "HKG", "PVG", "CDG", "DFW", "AMS", "FRA", "IST", "CAN", "JFK", "SIN", "DEN", "ICN", "BKK", "GRU", "YYZ"]
    let airportsPickerViewDataTo = ["ATL", "PEK", "DXB", "LAX", "HND", "ORD", "LHR", "HKG", "PVG", "CDG", "DFW", "AMS", "FRA", "IST", "CAN", "JFK", "SIN", "DEN", "ICN", "BKK", "GRU", "YYZ"]
    var forecastSpecificCall = "flights"
    var fromChoice = ""
    var toChoice = ""
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1{
            return airportsPickerViewDataFrom.count
        }else if pickerView.tag == 2{
            return airportsPickerViewDataTo.count
        }else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1{
            fromChoice = airportsPickerViewDataFrom[row]
        }else if pickerView.tag == 2{
            toChoice = airportsPickerViewDataTo[row]
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1{
            return airportsPickerViewDataFrom[row]
        }else if pickerView.tag == 2{
            return airportsPickerViewDataTo[row]
        }else{
            return ""
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if (WCSession.isSupported()) {
            print("Yes it is!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        
        fromPicker.delegate = self
        fromPicker.dataSource = self
        toPicker.delegate = self
        toPicker.dataSource = self
    }

    @IBAction func searchPressed(_ sender: Any) {
        print("criando conexao")
        
        print("from  = \(fromChoice)")
        print("to  = \(toChoice)")
        
        allFlights!.removeAll()
        
        let forecastService = ConnectAPI(specificCall: forecastSpecificCall)
        forecastService.getCurrentFlight(flyFrom: fromChoice, flyTo: toChoice, dateFrom: "18/03/2019", dateTo: "19/03/2019", currency: "CAD") { (currentFlight) in  
            data = currentFlight as! [Flight]
            //self.tableView.reloadData()
            print("data = \(data.count)")
            print("\(data[0].airlines)\(data[0].flightNbo)")
            print("fora -> \(allFlights)")
            
            for (key, value) in data.enumerated() {
                allFlights?.append(value)
            }
            
            print("fora 2 -> \(allFlights)")
            
            if (WCSession.default.isReachable) {
                print("entrei)")
                // construct the message you want to send
                // the message is in dictionary
                let message = ["from": self.fromChoice, "to": self.toChoice]
                // send the message to the watch
                WCSession.default.sendMessage(message, replyHandler: nil)
            }
        }
        //print(self.data[0]?.cityFrom)
        //print("-> \(data.count)")
        //print("-> \(data[0].airlines)")
        
        
    }
    
}

